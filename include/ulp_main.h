#include "Arduino.h"

extern uint32_t ulp_entry;

extern uint32_t ulp_led_delay;
//extern uint32_t ulp_low_thr;
extern uint32_t ulp_high_thr;
extern uint32_t ulp_sample_counter;
extern uint32_t ulp_last_result_rx;
extern uint32_t ulp_last_result_tx;
extern uint32_t ulp_last_result_diff;

