#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "sdkconfig.h"
#include <Arduino.h>
#include <WiFi.h>
#include <ESPmDNS.h>

#include "esp_sleep.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/sens_reg.h"
#include "soc/rtc_periph.h"
#include "driver/gpio.h"
#include "driver/rtc_io.h"
#include "driver/adc.h"
#include "driver/dac.h"
#include "esp32/ulp.h"
#include "ulp_main.h"
#include "crc.h"

#include "config.h"

#include "unistd.h"

#include <Arduino.h>
#include <EEPROM.h>
#include "Update.h"
//#include "crc.h"

// to fix error: no type named 'type' in 'struct ArduinoJson::Internals::EnableIf<false, String>'
#define ARDUINOJSON_ENABLE_ARDUINO_STRING 1
#include <ArduinoJson.h>

#include <WiFi.h>
#include <string>

#include <ESPAsyncWebServer.h>
#include <ESPAsync_WiFiManager.h>
#include <AsyncMqttClient.h>

extern const uint8_t ulp_main_bin_start[] asm("_binary_ulp_main_bin_start");
extern const uint8_t ulp_main_bin_end[] asm("_binary_ulp_main_bin_end");

static void init_ulp_program();
static void start_ulp_program();

#include <limits.h>
//extern unsigned long crc(uint8_t *blob, unsigned int size);

extern void app_loop();
extern void app_setup();

struct hostConfig myhostConfig;

AsyncMqttClient mqttClient;
AsyncWebServer server(80);
DNSServer dns;


//flag for saving data
bool shouldSaveConfig = false;
//callback notifying us of the need to save config
void saveConfigCallback()
{
    Serial.println(F("Save config\n"));
    shouldSaveConfig = true;
}

#define uS_TO_S_FACTOR 1000000 /* Conversion factor for micro seconds to seconds */
//#define DEFAULT_TIME_TO_SLEEP 900 /* Time ESP32 will go to sleep (in seconds) */

RTC_DATA_ATTR int bootCount = 0;
RTC_DATA_ATTR int32_t esp_timer_time = 0;
esp_sleep_wakeup_cause_t wakeup_reason;

const char *version = "0.1";
bool doing_update = false;
bool force_config = false;
long rssi;
bool has_valid_appconfig = false;
int last_mqtt_package_id = 0;
int last_publish_success_id = 0;
bool mqtt_do_reconnection = false;
const char *buildtime = (const char *)BUILDTIME;

unsigned long timeout_millis = 0;
void *jsonMessage;

bool setupWifiAndConfig(bool force)
{
    char get_ip[20] = "";
    char get_gw[20] = "";
    char get_sn[20] = "";
    char get_hostname[40] = "";
    char get_realname[40] = "";

    ESPAsync_WMParameter set_ip("IP", "ESP IP", get_ip, 20);
    ESPAsync_WMParameter set_gw("GW", "Gateway", get_gw, 20);
    ESPAsync_WMParameter set_sn("NM", "Netmasq", get_sn, 20);
    ESPAsync_WMParameter set_hostname("Hostname", "Hostname", get_hostname, 40);
    ESPAsync_WMParameter set_realname("Realname", "Realname", get_realname, 40);

    ESPAsync_WiFiManager wifiManager(&server, &dns);

    wifiManager.setConfigPortalTimeout(300);
    //set config save notify callback
    wifiManager.setSaveConfigCallback(saveConfigCallback);

    wifiManager.addParameter(&set_ip);
    wifiManager.addParameter(&set_gw);
    wifiManager.addParameter(&set_sn);
    wifiManager.addParameter(&set_hostname);
    wifiManager.addParameter(&set_realname);

    bool success = false;
    if (force)
    {
        wifiManager.resetSettings();
        success = wifiManager.startConfigPortal("OnDemandAP");
    }
    else
    {
        wifiManager.setSTAStaticIPConfig(myhostConfig._ip, myhostConfig._gw, myhostConfig._sn);
        success = wifiManager.autoConnect("ESPWifiConfig");
    }
    if (success && shouldSaveConfig)
    {
        myhostConfig._ip.fromString(String(set_ip.getValue()));
        myhostConfig._gw.fromString(String(set_gw.getValue()));
        myhostConfig._sn.fromString(String(set_sn.getValue()));
        strcpy(myhostConfig.hostName, set_hostname.getValue());
        strcpy(myhostConfig.realName, set_realname.getValue());
        myhostConfig.crc = crc((uint8_t *)&myhostConfig, sizeof(hostConfig));
        EEPROM.put(0, myhostConfig);
        EEPROM.commit();
        Serial.println(myhostConfig.crc);
    }

    return success;
}

void setup()
{
    // Maybe for Battery
    //WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector
    Serial.begin(115200);
    Serial.setDebugOutput(true);
    timeout_millis = millis();
    wakeup_reason = esp_sleep_get_wakeup_cause();
    //print_wakeup_reason();
    if (wakeup_reason == ESP_SLEEP_WAKEUP_TIMER)
    {
        Serial.println("Timer wakeup, doing stop ulp");
        // To disable the timer (effectively preventing the ULP program from running again)
        REG_CLR_BIT(RTC_CNTL_STATE0_REG, RTC_CNTL_ULP_CP_SLP_TIMER_EN);
        //get_ir_values();
    }
    if (wakeup_reason != ESP_SLEEP_WAKEUP_ULP) // && wakeup_reason != ESP_SLEEP_WAKEUP_TIMER)
    {
        Serial.println("Not ULP wakeup");
        // enable later/ init_ulp_program();
    }
    if (wakeup_reason == ESP_SLEEP_WAKEUP_ULP)
    {
      Serial.println("Deep sleep ULP wakeup");

      /*
        ulp_last_result_rx &= UINT16_MAX;
        ulp_last_result_tx &= UINT16_MAX;
        ulp_last_result_diff &= UINT16_MAX;

        ulp_ir_pre_maesure = ulp_last_result_rx;
        ulp_ir_maesure = ulp_last_result_tx;
        ulp_ir_diff = ulp_last_result_diff;

        Serial.printf("ULP did %d measurements since last reset\n", ulp_sample_counter & UINT16_MAX);
        Serial.printf("Thresholds:  high=%d\n", ulp_high_thr);
        Serial.printf("rx=%d , tx=%d, diff=%d\n", ulp_last_result_rx,
                      ulp_last_result_tx, ulp_last_result_diff);
                      */
    }

    EEPROM.begin(sizeof(hostConfig));// + sizeof(appConfig));

    Serial.print("Version: ");
    Serial.print(version);
    Serial.print(" ");
    Serial.println(buildtime);
    ++bootCount;

    Serial.println("Boot number: " + String(bootCount));

    EEPROM.get(0, myhostConfig);
    //EEPROM.get(sizeof(hostConfig), myappConfig);
/*
    if (myappConfig.crc == crc((uint8_t *)&myappConfig, sizeof(appConfig)))
    {
        Serial.println(F("Valid APP Config readed"));
        Serial.println(myappConfig.crc);
        Serial.println(myappConfig.ir_wakeup_difference);
        Serial.println(myappConfig.ir_action_difference);
        Serial.println(myappConfig.ir_action_pre_measure_min);
        Serial.println(myappConfig.ir_action_measure_max);
        Serial.println(myappConfig.ir_interval_measure_ms);
        Serial.println(myappConfig.ulp_led_delay_ms);
        Serial.println(myappConfig.core_led_delay_ms);
        Serial.println(myappConfig.first_power_on_duration_ms);
        Serial.println(myappConfig.power_on_duration_ms);
        Serial.println(myappConfig.max_power_on_interval);
        Serial.println(myappConfig.delay_after_max_power_on_interval_ms);
        Serial.println(myappConfig.seconds_to_sleep);
        Serial.println(myappConfig.seconds_goto_sleep);
        Serial.println(myappConfig.loop_delay_ms);
        has_valid_appconfig = true;
    }
    else
    {
        Serial.println(F("No Valid APP Config, settings defaults"));
        myappConfig.firmwaretopic[0] = 0;
        myappConfig.ir_wakeup_difference = DEFAULT_IR_WAKEUP_DIFFERENCE;
        myappConfig.ir_action_difference = DEFAULT_IR_ACTION_DIFFERENCE;
        myappConfig.ir_action_pre_measure_min = DEFAULT_IR_ACTION_PRE_MEASURE_MIN;
        myappConfig.ir_action_measure_max = DEFAULT_IR_ACTION_MEASURE_MAX;
        myappConfig.ir_interval_measure_ms = DEFAULT_IR_INTERVAL_MEASURE_MS;
        myappConfig.ulp_led_delay_ms = DEFAULT_ULP_LED_DELAY_MS;
        myappConfig.core_led_delay_ms = DEFAULT_CORE_LED_DELAY_MS;
        myappConfig.first_power_on_duration_ms = DEFAULT_FIRST_POWER_ON_DURATION_MS;
        myappConfig.power_on_duration_ms = DEFAULT_POWER_ON_DURATION_MS;
        myappConfig.max_power_on_interval = DEFAULT_MAX_POWER_ON_INTERVAL;
        myappConfig.delay_after_max_power_on_interval_ms = DEFAULT_DELAY_AFTER_MAX_POWER_ON_INTERVAL_MS;
        myappConfig.seconds_to_sleep = DEFAULT_TIME_TO_SLEEP;
        myappConfig.seconds_goto_sleep = DEFAULT_TIME_GOTO_SLEEP;
        myappConfig.loop_delay_ms = DEFAULT_LOOP_DELAY_MS;
    }
*/
    //power_on_duration = myappConfig.first_power_on_duration_ms;

    if (myhostConfig.crc == crc((uint8_t *)&myhostConfig, sizeof(hostConfig)))
    {
        Serial.println(F("Valid Host Config readed"));
        Serial.println(myhostConfig.crc);
/*
        mqttClient.onConnect(onMqttConnect);
        mqttClient.onDisconnect(onMqttDisconnect);
        mqttClient.onSubscribe(onMqttSubscribe);
        mqttClient.onUnsubscribe(onMqttUnsubscribe);
        mqttClient.onMessage(onMqttMessage);
        mqttClient.onPublish(onMqttPublish);
        WiFi.onEvent(WiFiEvent);

        mqttClient.setServer(myhostConfig.mqttHost, myhostConfig.mqttPort);
        //wifiManager.autoConnect(myhostConfig.hostName);
        if (!setupWifiAndConfig(false))
        {
            Serial.println("failed to connect and hit timeout");
            //goSleep();
        }
        */
    }
    else
    {
        Serial.println(F("Invalid Config readed"));
        Serial.println(myhostConfig.crc);

        if (!setupWifiAndConfig(true))
        {
            Serial.println("failed to connect and hit timeout");
            //delay(3000);
            //reset and try again, or maybe put it to deep sleep
            //setSleepTime(10);
            //goSleep();
        }
    }

    if (shouldSaveConfig)
    {
        //setSleepTime(10);
        //goSleep();
    }

    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.print("ESP Mac Address: ");
    Serial.println(WiFi.macAddress());
    Serial.print("Subnet Mask: ");
    Serial.println(WiFi.subnetMask());
    Serial.print("Gateway IP: ");
    Serial.println(WiFi.gatewayIP());
    app_setup();
}

void loop()
{
    if (force_config)
    {
        setupWifiAndConfig(true);
        doing_update = false;
    }
    /*
    if (myappConfig.loop_delay_ms >= 100)
    {
        Serial.print(millis());
        Serial.print(" in loop. ");
    }
    if (mqtt_do_reconnection)
    {
        connectToMqtt();
    }
    if (mqttClient.connected() && send_action_message)
    {
        publishMotorAction();
    }
    */

/*
    if ((timeout_millis + myappConfig.seconds_goto_sleep * 1000) < millis() ||
        (millis() > (MAX_TOTAL_RUNNING_SECONDS * 1000)))
    {
        Serial.println("Timeout, go sleep");
        setSleepTime(myappConfig.seconds_to_sleep);
        digitalWrite(PUMP, LOW);
        pump_state = false;
        goSleep();
    }
    */
    //delay(myappConfig.loop_delay_ms);
    //delay(1000);
    app_loop();
}

static void init_ulp_program()
{
    // load the assembled tcrt5000.S code into the ulp
    esp_err_t err = ulp_load_binary(0, ulp_main_bin_start,
                                    (ulp_main_bin_end - ulp_main_bin_start) / sizeof(uint32_t));
    ESP_ERROR_CHECK(err);

    //ulp_set_wakeup_period(0, ULP_IR_SCAN_INTERVAL * 1000);

    rtc_gpio_isolate(GPIO_NUM_12);
    rtc_gpio_isolate(GPIO_NUM_15);
    //esp_deep_sleep_disable_rom_logging(); // suppress boot messages
}

static void start_ulp_program()
{
    /* Configure ADC channel */
    /* Note: when changing channel here, also change 'adc_channel' constant
       in tcrt5000.S */
    //adc1_config_channel_atten(ADC1_CHANNEL_6, ADC_ATTEN_DB_11);
    //adc1_config_width(ADC_WIDTH_BIT_12);
    //adc1_ulp_enable();

    /* Set low and high thresholds */
    //ulp_high_thr = myappConfig.ir_wakeup_difference;
    //ulp_high_thr = myappConfig.maximum_ir_thr;
    //ulp_led_delay = myappConfig.ulp_led_delay_ms;

    /* Reset sample counter */
    ulp_sample_counter = 0;
    // now start the ulp coprocessor running separately
    esp_err_t err = ulp_run((&ulp_entry - RTC_SLOW_MEM) / sizeof(uint32_t));
    ESP_ERROR_CHECK(err);
}
