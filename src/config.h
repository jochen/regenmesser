#include <Arduino.h>

#define DEFAULT_HOSTNAME "regenmesser"

#define CONFIG_CHAR_LENGTH 40

#define POWER_PIN D8

extern const char *version;
extern const char *buildtime;

struct hostConfig
{
    IPAddress _ip;
    IPAddress _gw;
    IPAddress _sn;
    char hostName[CONFIG_CHAR_LENGTH];
    char realName[CONFIG_CHAR_LENGTH];
    unsigned long crc;
};

struct logicDebugPin {
  const char *name;
  uint8_t pin;
};

#define MAXTOPICLENGTH 128
#define MAXPAYLOADLENGTH 30
#define MAXURLLENGHT 128

/* App configuration */

struct appConfig
{
  char mqttTopicprefix[CONFIG_CHAR_LENGTH] = {};
  char mqttHost[CONFIG_CHAR_LENGTH] = {};
  unsigned int mqttPort = 0;

  //float R2_CAL[SENSORCOUNT] = {};
  unsigned long crc = 0;
};

