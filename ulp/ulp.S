/* ULP assembly files are passed through C preprocessor first, so include directives
   and C macros may be used in these files 
 */
#include "soc/rtc_cntl_reg.h"
#include "soc/soc_ulp.h"
#include "soc/rtc_io_reg.h"  // for RTC_GPIO_*

/* example pins
.set gpio_2, 12              // gpio pin 2 is rtc pin 12
.set gpio_32, 9
.set gpio_25, 6              // gpio pin 32 is rtc pin 9
*/
	.set gpio_27, 17
	.set gpio_led_rx, gpio_27

	.set gpio_13, 14
	.set gpio_led_tx, gpio_13

	/* ADC1 channel 6, GPIO34 */
	.set adc_channel, 6

	/* Configure the number of ADC samples to average on each measurement.
	   For convenience, make it a power of 2. */
	.set adc_oversampling_factor_log, 2
	.set adc_oversampling_factor, (1 << adc_oversampling_factor_log)

	/* Define variables, which go into .bss section (zero-initialized data) */
	.bss

	.global led_delay
led_delay:
	.long 0

	/* Low threshold of ADC reading.
	   Set by the main program. */
	/*.global low_thr
low_thr:
	.long 0*/

	/* High threshold of ADC reading.
	   Set by the main program. */
	.global high_thr
high_thr:
	.long 0

	/* Counter of measurements done */
	.global sample_counter
sample_counter:
	.long 0

	.global last_result_rx
last_result_rx:
	.long 0

	.global last_result_tx
last_result_tx:
	.long 0

	.global last_result_diff
last_result_diff:
	.long 0

	/* Code goes into .text section */
	.text
	.global entry


entry:
	// might be needed for some pads, but surely not #2
	// WRITE_RTC_REG(RTC_IO_TOUCH_PAD2_REG, RTC_IO_TOUCH_PAD2_TO_GPIO_S, 1, 1)
	//WRITE_RTC_REG(RTC_IO_TOUCH_PAD4_REG, RTC_IO_TOUCH_PAD4_TO_GPIO_S, 1, 1)

	// use digital function, not rtc function
	// WRITE_RTC_REG(RTC_IO_TOUCH_PAD2_REG, RTC_IO_TOUCH_PAD2_MUX_SEL_S, 1, 1)
	WRITE_RTC_REG(RTC_IO_TOUCH_PAD7_REG, RTC_IO_TOUCH_PAD7_MUX_SEL_S, 1, 1)
	WRITE_RTC_REG(RTC_IO_TOUCH_PAD4_REG, RTC_IO_TOUCH_PAD4_MUX_SEL_S, 1, 1)

	// gpio_* shall be output, not input
	WRITE_RTC_REG(RTC_GPIO_OUT_REG, RTC_GPIO_OUT_DATA_S + gpio_led_rx, 1, 1)
	WRITE_RTC_REG(RTC_GPIO_OUT_REG, RTC_GPIO_OUT_DATA_S + gpio_led_tx, 1, 1)

	/* power on rx led */
	WRITE_RTC_REG(RTC_GPIO_ENABLE_W1TS_REG, RTC_GPIO_ENABLE_W1TS_S + gpio_led_rx, 1, 1)

	move  r1, led_delay     // wait in ms
	ld r1, r1, 0
	move  r2, pre_init_adc_rx  // return address
	jump  delay             // call delay subroutine

pre_init_adc_rx:

	/* increment sample counter */
	move r3, sample_counter
	ld r2, r3, 0
	add r2, r2, 1
	st r2, r3, 0

	/* do measurements using ADC */
	/* r0 will be used as accumulator */
	move r0, 0
	/* initialize the loop counter */
	stage_rst
measure_rx:
	/* measure and add value to accumulator */
	adc r1, 0, adc_channel + 1
	add r0, r0, r1
	/* increment loop counter and check exit condition */
	stage_inc 1
	jumps measure_rx, adc_oversampling_factor, lt

	/* divide accumulator by adc_oversampling_factor.
	   Since it is chosen as a power of two, use right shift */
	rsh r0, r0, adc_oversampling_factor_log
	/* averaged value is now in r0; store it into last_result */
	move r3, last_result_rx
	st r0, r3, 0

	/* power on tx led */
	WRITE_RTC_REG(RTC_GPIO_ENABLE_W1TS_REG, RTC_GPIO_ENABLE_W1TS_S + gpio_led_tx, 1, 1)

	move  r1, led_delay     // wait in ms
	ld r1, r1, 0
	move  r2, pre_init_adc_tx  // return address
	jump  delay             // call delay subroutine

pre_init_adc_tx:

	/* do measurements using ADC */
	/* r0 will be used as accumulator */
	move r0, 0
	/* initialize the loop counter */
	stage_rst
measure_tx:
	/* measure and add value to accumulator */
	adc r1, 0, adc_channel + 1
	add r0, r0, r1
	/* increment loop counter and check exit condition */
	stage_inc 1
	jumps measure_tx, adc_oversampling_factor, lt

	/* divide accumulator by adc_oversampling_factor.
	   Since it is chosen as a power of two, use right shift */
	rsh r0, r0, adc_oversampling_factor_log
	/* averaged value is now in r0; store it into last_result */
	move r3, last_result_tx
	st r0, r3, 0


	/* power off led*/
	WRITE_RTC_REG(RTC_GPIO_ENABLE_W1TC_REG, RTC_GPIO_ENABLE_W1TC_S + gpio_led_tx, 1, 1)
	WRITE_RTC_REG(RTC_GPIO_ENABLE_W1TC_REG, RTC_GPIO_ENABLE_W1TC_S + gpio_led_rx, 1, 1)
	
	/* load rx value and substract tx value from it */
	move r3, last_result_rx
	ld r3, r3, 0
	sub r0, r3, r0

	move r3, last_result_diff
	st r0, r3, 0
	jump exit, ov


	/* compare with low_thr; wake up if value < low_thr */
	/* move r3, low_thr
	ld r3, r3, 0
	sub r3, r0, r3
	jump wake_up, ov*/

	/* compare with high_thr; wake up if value > high_thr */
	move r3, high_thr
	ld r3, r3, 0
	sub r3, r3, r0
	jump wake_up, ov

	/* value within range, end the program */
	.global exit
exit:
	halt

	.global wake_up
wake_up:
	/* Check if the system can be woken up */
	READ_RTC_FIELD(RTC_CNTL_LOW_POWER_ST_REG, RTC_CNTL_RDY_FOR_WAKEUP)
	and r0, r0, 1
	jump exit, eq

	/* Wake up the SoC, end program */
	wake
	WRITE_RTC_FIELD(RTC_CNTL_STATE0_REG, RTC_CNTL_ULP_CP_SLP_TIMER_EN, 0)
	halt

delay:
  wait  8000              // wait 8000 clock ticks at 8MHz -> 1ms
  sub   r1, r1, 1         // decrement ms count
  jump  r2, eq            // if ms count is zero then return to caller
  jump  delay             // else continue to wait
